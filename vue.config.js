module.exports = {
  configureWebpack: {
    devServer: {
      proxy: "https://back-vercel-6xnjs5569-jcguzmar02.vercel.app",
      headers: { "Access-Control-Allow-Origin": "*" },
    },
  },
};
